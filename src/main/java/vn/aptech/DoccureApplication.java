package vn.aptech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoccureApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoccureApplication.class, args);
	}

}
